// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopdownShooterGameMode.h"
#include "TopdownShooterPlayerController.h"
#include "../Character/TopdownShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATopdownShooterGameMode::ATopdownShooterGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATopdownShooterPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("Blueprint'/Game/Blueprint/Character/BP_Character'"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}